#!/bin/bash

#main code

if ! [ -f ~/.ssh/config ] 
then
	echo "Ssh config file [~/.ssh/config] not exist.";
	exit 0;
fi;


#SSH_HOSTS=($(sed -ne 's/^Host //p' < ~/.ssh/config));
SSH_HOSTS=($(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2-));

i=1;
echo "Connect..."
for o in "${SSH_HOSTS[@]}"; do
	echo "$i - $o";
	let i++;	
done
echo "";
echo "e - edit ssh config file";
echo "";
read -p "Select: " opt;

if [[ -z $opt ]]; 
then 
	exit;	
elif [[ "$opt" == "e" ]]
then
	vim ~/.ssh/config;
elif [[ "$opt" =~ ^[0-9]+$ ]] && ! [[ -z "${SSH_HOSTS[$opt-1]}" ]];
then
	SelectedOpt=${SSH_HOSTS[$opt-1]};
	echo "ssh $SelectedOpt";
	ssh $SelectedOpt;
else
	echo "Invalid option";
fi;
