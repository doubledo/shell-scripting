#!/bin/bash

if ! [ -f ~/.ssh/config ] 
then
	echo "Ssh config file [~/.ssh/config] not exist.";
	exit 0;
fi;


SSH_GROUPS=($(grep -i "^\s*[#]\s*groups [a-z_ -]*" ~/.ssh/config | cut -d " " -f2- | tr ' ' '\n' | sort -u | tr '\n' ' '));

i=1;
echo "Connect..."
for g in "${SSH_GROUPS[@]}"; do
	echo "$i - [ $g ]";
	let i++;
done

HOSTS_WO_GROUP=($(
/usr/bin/awk '
        BEGIN { FS="\n"; RS=""; ORS=" "; }
        {
                for(n=1; n<=NF; n++) {
                        if($n ~ /^[Hh][Oo][Ss][Tt] / && $n !~ /[?*]/) {
                                inGroup = false
                                if ($0 !~ /^\#[Gg][Rr][Oo][Uu][Pp][Ss]/) {
                                        host = $n; sub(/^[Hh][Oo][Ss][Tt] /, "", host);
                                        print host;
                                }
                        }
                }
        }' ~/.ssh/config));

for h in "${HOSTS_WO_GROUP[@]}"; do
        echo "$i - $h";
        let i++;
done

echo "";
echo "a - all";
echo "e - edit ssh config file";
echo "";
read -p "Select: " opt;

if [[ -z $opt ]];
then
	exit;
elif [[ "$opt" == "e" ]]
then
	vim ~/.ssh/config;
elif [[ "$opt" == "a" ]]
then
	SSH_HOSTS=($(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2-));
	j=1;
	for h in "${SSH_HOSTS[@]}"; do
		echo "$j - $h";
		let j++;
	done
	echo "";
	read -p "Select: " opt;
	
	if [[ "$opt" =~ ^[0-9]+$ ]] && ! [[ -z "${SSH_HOSTS[$opt-1]}" ]];
	then
		SelectedHost=${SSH_HOSTS[$opt-1]};
		echo "ssh $SelectedHost";
		ssh $SelectedHost;
	else
		echo "Invalid option";
	fi;

elif [[ "$opt" =~ ^[1-9][0-9]*$ ]] && ! [[ -z "${SSH_GROUPS[$opt-1]}" ]];
then
	SelectedGroup=${SSH_GROUPS[$opt-1]};
	echo "";
	echo "[ $SelectedGroup ]";
	GROUP_HOSTS=($(
	/usr/bin/awk '
		BEGIN { FS="\n"; RS=""; ORS=" "; }
		{
			for(n=1; n<=NF; n++) {
				if($n ~ /^\#[Gg][Rr][Oo][Uu][Pp][Ss].* '$SelectedGroup'([ ]|$)/) {
					for(m=1; m<=NF; m++) {
						if($m ~ /^[Hh][Oo][Ss][Tt] / && $m !~ /[?*]/) {
							host = $m; sub(/^[Hh][Oo][Ss][Tt] /, "", host);
							print host;
						}
					}
				}
			}
		}' ~/.ssh/config));
	j=1;
	for h in "${GROUP_HOSTS[@]}"; do
		echo "$j - $h";
		let j++;
	done
	echo "";
	read -p "Select: " opt;
	if [[ "$opt" =~ ^[1-9][0-9]*$ ]] && ! [[ -z "${GROUP_HOSTS[$opt-1]}" ]];
	then
		SelectedHost=${GROUP_HOSTS[$opt-1]};
		echo "ssh $SelectedHost";
		ssh $SelectedHost;
	else
		echo "Invalid option";
	fi; 
elif [[ "$opt" =~ ^[1-9][0-9]*$ ]] && ! [[ -z "${HOSTS_WO_GROUP[$opt-${#SSH_GROUPS[@]}-1]}" ]];
then
	SelectedHost=${HOSTS_WO_GROUP[$opt-${#SSH_GROUPS[@]}-1]};
	echo "ssh $SelectedHost";
	ssh $SelectedHost;
else
	echo "Invalid option";
fi;
