# Zbierka mojích shell scriptov

#### vbox-tool.sh
skript pre zjednodušenú prácu s virtuálnymi mašinami cez príkazový riadok

#### ssh-connect.sh
jednoduchý skript pre ssh pripojenie pomocou prehľadného zoznamu pripojení zadefinovaných v **~/.ssh/config** súbore
