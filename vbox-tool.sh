#!/bin/bash

array_contains() { 
	local array="$1[@]"
	local seeking=$2
	local in=1
	for element in "${!array}"; do
		if [[ $element == $seeking ]]; then
			in=0
			break
		fi
	done
	return $in
}

declare -a VMs;
declare -a VMIDs;
declare -a VMOpt;
i=0;


createVM() {
	VM="rebel-vbox2";
	VBROOT="/media/data/vbox/";
	ISO="/media/data/iso/xubuntu-18.04-desktop-amd64.iso";
	OSTYPE="Ubuntu_64";
	DISKSIZE=5120; # MB
	RAM=1024; # MB
	CPU=1;
	CPUCAP=100;
	MACADDRESS=000011112222
	NETWORKINTERFACE="eno1"
	VRDEPORT=3389

	PAE="on";
	HWVIRT="on";
	NESTPAGING="on";
	VRAM=8;
	USB="off";

	read -p "VM name [$VM]: " userInput; VM=${userInput:-$VM}; unset userInput
	read -p "VB root folder [$VBROOT]: " userInput; VBROOT=${userInput:-$VBROOT}; unset userInput
	read -p "DVD ISO image [$ISO]: " userInput; ISO=${userInput:-$ISO}; unset userInput
	read -p "OS type [$OSTYPE]: " userInput; OSTYPE=${userInput:-$OSTYPE}; unset userInput
	read -p "Hard drive size in MB [$DISKSIZE]: " userInput; DISKSIZE=${userInput:-$DISKSIZE}; unset userInput
	read -p "RAM size in MB [$RAM]: " userInput; RAM=${userInput:-$RAM}; unset userInput
	read -p "CPU cores [$CPU]: " userInput; CPU=${userInput:-$CPU}; unset userInput
	read -p "CPU exec cap [$CPUCAP]: " userInput; CPUCAP=${userInput:-$CPUCAP}; unset userInput
	echo;
	netstat -i
	read -p "Network host interface [$NETWORKINTERFACE]: " userInput; NETWORKINTERFACE=${userInput:-$NETWORKINTERFACE}; unset userInput
	read -p "Network MAC address [$MACADDRESS]: " userInput; MACADDRESS=${userInput:-$MACADDRESS}; unset userInput
	read -p "VRDE port [$VRDEPORT]: " userInput; VRDEPORT=${userInput:-$VRDEPORT}; unset userInput
	
	echo
	echo "New VM overview..."
	echo
	echo "VM name: $VM"
	echo "VB root folder: $VBROOT"
	echo "DVD ISO image: $ISO"
	echo "OS type: $OSTYPE"
	echo "Hard drive size in MB: $DISKSIZE"
	echo "RAM size in MB: $RAM"
	echo "CPU cores: $CPU"
	echo "CPU exec cap: $CPUCAP"
	echo "Network host interface: $NETWORKINTERFACE"
	echo "Network MAC address: $MACADDRESS"
	echo "VRDE port: $VRDEPORT"

	read -p "Everithing OK [yes/NO]: " userInput;
	if [ "$userInput" == "yes" ]; then
		echo "Creating the "$VM" VM."
		VBoxManage createvm --register --name "$VM" --basefolder "$VBROOT" --ostype "$OSTYPE";
		VBoxManage createmedium disk --filename "$VBROOT"/"$VM"/"$VM".vdi --size "$DISKSIZE";
		VBoxManage storagectl "$VM" --name "$VM SATA Controller" --add sata --controller IntelAHCI;
		VBoxManage storageattach "$VM" --storagectl "$VM SATA Controller" --port 0 --device 0 --type hdd --medium "$VBROOT"/"$VM"/"$VM".vdi;
		VBoxManage storagectl "$VM" --name "$VM IDE Controller" --add ide;
		VBoxManage storageattach "$VM" --storagectl "$VM IDE Controller" --port 0 --device 0 --type dvddrive --medium "$ISO";
		VBoxManage modifyvm "$VM" --memory "$RAM";
		VBoxManage modifyvm "$VM" --boot1 dvd --boot2 disk --boot3 none --boot4 none;
		VBoxManage modifyvm "$VM" --chipset piix3;
		VBoxManage modifyvm "$VM" --ioapic on;
		VBoxManage modifyvm "$VM" --mouse ps2;
		VBoxManage modifyvm "$VM" --cpus "$CPU" --cpuexecutioncap "$CPUCAP" --pae "$PAE";
		VBoxManage modifyvm "$VM" --hwvirtex on --nestedpaging on;
		VBoxManage modifyvm "$VM" --nic1 bridged --bridgeadapter1 "$NETWORKINTERFACE" --macaddress1 "$MACADDRESS";
		VBoxManage modifyvm "$VM" --vram "$VRAM";
		#VBoxManage modifyvm "$VM" --monitorcount 1;
		#VBoxManage modifyvm "$VM" --accelerate2dvideo off --accelerate3d off;
		VBoxManage modifyvm "$VM" --audio none;
		VBoxManage modifyvm "$VM" --snapshotfolder "$VBROOT"/"$VM"/Snapshots;
		VBoxManage modifyvm "$VM" --clipboard bidirectional;
		VBoxManage modifyvm "$VM" --usb "$USB";
		VBoxManage modifyvm "$VM" --vrde on
		#VBoxManage modifyvm "$VM" --vrdeauthtype external
		VBoxManage modifyvm "$VM" --vrdeport $VRDEPORT

		echo "VM created";
	fi
	unset userInput
}

selectSnapshotAction() {
	echo ;
	echo "Snapshots for VM $SelectedVM - $SelectedVMId ";

	local -a VMSnaps;
	local -a VMSnapIds;
	local -a VMSnapOpts;

	local lineRegex='^[[:space:]]*Name: (.*) [(]UUID[:] ([^)]*)[)](.*)$';
	local i=0;
	while read -r line
	do
		if [[ $line =~ $lineRegex ]]; then
			VMSnaps[$i]=${BASH_REMATCH[1]};
			VMSnapIDs[$i]=${BASH_REMATCH[2]};
			VMSnapOpts[$i]="${BASH_REMATCH[1]} - ${BASH_REMATCH[2]} ${BASH_REMATCH[3]}";
			((i++));
		fi
	done < <(VBoxManage snapshot $SelectedVMId list | grep UUID )

	local createNewSnap='Take new VM snapshot';
	VMSnapOpts[$i]="$createNewSnap";

	i=1;
	for o in "${VMSnapOpts[@]}"; do
		echo "$i - $o"
		let i++
	done
	read -p "Select: " opt;

	if ! [[ "$opt" =~ ^[0-9]*$ ]]; then exit; fi;

	SelectedOpt=${VMSnapOpts[$opt-1]};
	if [[ -z $opt ]]; then
		return;
	elif [[ -z $SelectedOpt ]]; then
		exit;
	elif [[ ${VMSnapOpts[$opt-1]} == $createNewSnap ]]; then
		echo ;
		echo "Create new snapshot...";
		read -p "Snapshot name: " SnapName
		if [[ -z $SnapName ]]; then
			exit 1;
		fi
		read -p "Snapshot description: " SnapDesc
		read -p "Take live snap [yes/NO]: " SnapLive

		if [[ "$SnapLive" == "yes" ]]; then
			VBoxManage snapshot $SelectedVMId take "$SnapName" --description "$SnapDesc" --live
		else
			VBoxManage snapshot $SelectedVMId take "$SnapName" --description "$SnapDesc"
		fi
	else
		SelectedVMSnap=${VMSnaps[$opt-1]};
		SelectedVMSnapId=${VMSnapIDs[$opt-1]};
		echo
		echo "Snapshot $SelectedVMSnap";
		echo "1 - snapshot VM info"
		echo "2 - restore snapshot"
		echo "3 - delete snapshot"
		read -p "Select: " opt2
		case $opt2 in
			"1")
				VBoxManage snapshot $SelectedVMId showvminfo $SelectedVMSnapId | less
				;;
			"2")
				read -p "Are you realy sure to restore $SelectedVM with snapshot $SelectedVMSnap [yes/NO]: " userInput;
				if [ "$userInput" == "yes" ]; then
					echo "Restoring VM $SelectedVM...";
					VBoxManage snapshot $SelectedVMId restore $SelectedVMSnapId
				fi
				unset userInput
				;;
			"3")
				read -p "Are you realy sure to remove snapshot $SelectedVMSnap [yes/NO]: " userInput;
				if [ "$userInput" == "yes" ]; then
					echo "Removing snapshot $SelectedVMSnap...";
					VBoxManage snapshot $SelectedVMId delete $SelectedVMSnapId
				fi
				unset userInput
				;;
			"")
				echo
				;;
			*)
				exit 1;
				;;
		esac
	fi

}

selectOtherAction() {
	options3=("VM Storages" "Snapshots" "VRDE options" "Remove VM")
	while true; do
		echo ;
		echo "Selected VM $SelectedVM - $SelectedVMId";
		i=1;
		for o in  "${options3[@]}"; do
			echo "$i - $o"			
			let i++
		done

		read -p "Select action: " reply

		case $reply in
			"1"|"${options3[0]}") 
				echo "NOT DONE YET...."; 
				# VBoxManage storageattach $SelectedVMId --storagectl "rebel-vbox-ide-ctrlr" --port 1 --device 0 --type dvddrive --medium none
				break;;
			"2"|"${options3[1]}")
				selectSnapshotAction;
				break;;
			"3"|"${options3[2]}")
				echo "NOT DONE YET....";
				break;;
			"4"|"${options3[3]}")
				read -p "Are you realy sure to remove $SelectedVM with everithing [yes/NO]: " userInput;
				if [ "$userInput" == "yes" ]; then
					echo "Removing VM $SelectedVM...";
					VBoxManage unregistervm $SelectedVMId --delete
				fi
				unset userInput
				return 0
				;;
			"") break;;
			*) exit 1 ;;
		esac
	done;
}

selectAction() {
	options2=("Info" "Start headless" "Pause" "Resume" "Reset" "Power off" "Others...")
	while true; do
		echo;
		echo "Selected VM $SelectedVM - $SelectedVMId";

		i=1;
		for o in  "${options2[@]}"; do
			echo "$i - $o"			
			let i++
		done
		
		read -p "Select action: " reply

		case $reply in
			"1"|"${options2[0]}") 
				VBoxManage showvminfo $SelectedVMId | less;
				break;;
			"2"|"${options2[1]}")
				VBoxManage startvm $SelectedVMId --type headless;
				break;;
			"3"|"${options2[2]}")
				VBoxManage controlvm $SelectedVMId pause;
				break;;
			"4"|"${options2[3]}")
				VBoxManage controlvm $SelectedVMId resume;
				break;;
			"5"|"${options2[4]}")
				VBoxManage controlvm $SelectedVMId reset;
				break;;
			"6"|"${options2[5]}")
				VBoxManage controlvm $SelectedVMId poweroff;
				break;;
			"7"|"${options2[6]}")
				selectOtherAction;
				break;;
			"") break;;
			*) exit 1 ;;
		esac
	done;
}

# MAIN code...
while true; do
	lineRegex='^["]([^"]*)["][[:space:]]+[{]([^}]*)[}]$';
	i=0;
	unset VMs;
	unset VMIDs;
	unset VMOpt;
	while read -r line
	do
		if [[ $line =~ $lineRegex ]]; then
			VMs[$i]=${BASH_REMATCH[1]};
			VMIDs[$i]=${BASH_REMATCH[2]};
			VMOpt[$i]="${BASH_REMATCH[1]} - ${BASH_REMATCH[2]} - `vboxmanage showvminfo ${BASH_REMATCH[2]} | grep State | sed 's/State:[ ]*//'`";
			((i++));
		fi
	done < <(vboxmanage list vms;)

	createNewVM='Create new VM';
	VMOpt[$i]="$createNewVM";

	echo ;
	echo "VMS list...";

	i=1;
	for o in "${VMOpt[@]}"; do
		echo "$i - $o"                  
		let i++
	done
	read -p "Select VM: " opt;

	if ! [[ "$opt" =~ ^[0-9]+$ ]]; then exit; fi;

	SelectedOpt=${VMOpt[$opt-1]};
	if [[ -z $SelectedOpt ]]; then 
		exit; 
	elif [[ ${VMOpt[$opt-1]} == $createNewVM ]]; then
		createVM;
	else
		SelectedVM=${VMs[$opt-1]};
		SelectedVMId=${VMIDs[$opt-1]};
		selectAction;
	fi
done

